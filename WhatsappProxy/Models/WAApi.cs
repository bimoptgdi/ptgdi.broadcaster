﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace WhatsappProxy.Models
{
    public class WAApi
    {
        private string APIUrl = "https://eu135.chat-api.com/instance147166/";
        private string token = "qtzpme4ybi8u64dh";
        private IConfigurationSection configurationSection;

        public WAApi()
        {
        }

        public WAApi(string aPIUrl, string token)
        {
            APIUrl = aPIUrl;
            this.token = token;
        }

        public WAApi(IConfigurationSection configurationSection)
        {
            this.configurationSection = configurationSection;
        }

        public async Task<string> SendRequest(string method, string data)
        {
            string url = $"{APIUrl}{method}?token={token}";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                var result = await client.PostAsync("", content);
                return await result.Content.ReadAsStringAsync();
            }
        }

        //public async Task<LastMessage> GetMessages(int lastMessageNumber, int limit, string chatId)
        //{
        //    //string url = $"{APIUrl}messages?lastMessageNumber={lastMessageNumber}&limit={limit}";

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(APIUrl);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(
        //            new MediaTypeWithQualityHeaderValue("application/json"));

        //        HttpResponseMessage responseMessage = await client.GetAsync($"messages?&token={token}&lastMessageNumber={lastMessageNumber}&limit={limit}&chatId={chatId}");
        //        if (responseMessage.IsSuccessStatusCode)
        //        {
        //            return JsonConvert.DeserializeObject<LastMessage>(await responseMessage.Content.ReadAsStringAsync());
        //        }
        //        else
        //            return null;
        //    }
        //}

        public async Task<string> SendMessagesByPhone(string phoneNumber, string text)
        {
            var ids = phoneNumber.Split(',');
            string m = "";
            foreach(var i in ids)
            {
                var data = new Dictionary<string, string>()
                {
                    {"phone",i },
                    { "body", text }
                };
                m = await SendRequest("sendMessage", JsonConvert.SerializeObject(data));
            }
            return m;
        }

        public async Task<string> SendMessage(string chatID, string text)
        {
            var data = new Dictionary<string, string>()
            {
                {"chatId",chatID },
                { "body", text }
            };
            return await SendRequest("sendMessage", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendMessageByPhone(string phoneNumber, string text)
        {
            var data = new Dictionary<string, string>()
            {
                { "phone", phoneNumber },
                { "body", text }
            };
            return await SendRequest("sendMessage", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendOgg(string chatID, string ogg)
        {
            var data = new Dictionary<string, string>
            {
                {"audio", ogg },
                {"chatId", chatID }
            };

            return await SendRequest("sendAudio", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendGeo(string chatID, Double lat, Double lon, string caption)
        {
            var data = new Dictionary<string, string>()
            {
                { "lat", lat.ToString() },
                { "lng", lon.ToString() },
                { "address", caption },
                { "chatId", chatID}
            };
            return await SendRequest("sendLocation", JsonConvert.SerializeObject(data));

        }

        public async Task<string> CreateGroup(string author, string groupName, string messageText = "Welcome to this group!")
        {
            var phone = author.Replace("@c.us", "");
            var data = new Dictionary<string, string>()
            {
                { "groupName", groupName},
                { "phones", phone },
                { "messageText", messageText }
            };
            return await SendRequest("group", JsonConvert.SerializeObject(data));
        }

        public async Task<string> SendFile(string chatID, string path)
        {
            var data = new Dictionary<string, string>(){
                    { "chatId", chatID },
                    { "body", Converter.FileToBase64(path) },
                    { "filename", "yourfile" },
                    { "caption", $"Your file" }
                };

            return await SendRequest("sendFile", JsonConvert.SerializeObject(data));
        }
    }
}