﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WhatsappProxy.Models
{
    public class Converter
    {
        public static string FileToBase64(string fullPathToImage)
        {
            Byte[] bytes = File.ReadAllBytes(fullPathToImage);
            String base64Encoded = Convert.ToBase64String(bytes);
            return base64Encoded;
        }

        //public static string ListTimeSheetToListString(IEnumerable<TimeSheet> timeSheets)
        //{
        //    string result = string.Empty;
        //    int order = 1;
        //    foreach (TimeSheet timeSheet in timeSheets)
        //    {
        //        if (timeSheet != null)
        //        {
        //            string newLine = timeSheet.Task.Contains("<br>") ? "\n - " : "";
        //            result += $"{order}. [{timeSheet.Date:dd/MM/yyyy}] {newLine}{timeSheet.Task.Replace("<br>", "\n - ")}\n";
        //            order++;
        //        }
        //    }
        //    return string.IsNullOrEmpty(result) ? MessageConstant.NotExist : result;
        //}

        public static string DictStringStringToListString(Dictionary<string, string> pairs)
        {
            string result = string.Empty;
            int order = 1;
            foreach (KeyValuePair<string, string> pair in pairs)
            {
                result += $"{order}. *{pair.Key}* - {pair.Value}\n";
                order++;
            }
            return result;
        }
    }
}