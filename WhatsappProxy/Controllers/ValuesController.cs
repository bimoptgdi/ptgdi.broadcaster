﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WhatsappProxy.Models;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace WhatsappProxy.Controllers
{
    public class ValuesController : ApiController
    {
        private WAApi api;

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // GET api/values/text
        [HttpGet]
        [ActionName("SendMessages")]
        public async Task SendMessages(string param, string dst, string msg)
        {
            api = new WAApi();

            //parse message
            await api.SendMessagesByPhone(dst, msg);
            //return param+" "+dst + " " +msg;
        }

        // GET api/values/text
        [HttpGet]
        [ActionName("SendMessage")]
        public async Task SendMessage(string param, string dst, string msg)
        {
            api = new WAApi();

            //parse message
            await api.SendMessageByPhone(dst,msg);
            //return param+" "+dst + " " +msg;
        }

        // POST api/values
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        public List<Dictionary<string, dynamic>> GetJsonDataFromWebApi(string url)
        {
            try
            {
                List<Dictionary<string, dynamic>> output;

                Uri uri = new Uri(url);
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.UseDefaultCredentials = true;

                using (Stream requestStream = webRequest.GetResponse().GetResponseStream())
                {
                    StreamReader reader = new StreamReader(requestStream, System.Text.Encoding.UTF8);
                    string json = reader.ReadToEnd();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    output = serializer.Deserialize<List<Dictionary<string, dynamic>>>(json);
                }

                return output;
            }
            catch (WebException ex)
            {
                String errorText = "";
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();


                }
                throw new WebException(errorText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
